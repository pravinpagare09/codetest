﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Agl.Model;

namespace Agl.DataAccess.Interface
{
    public interface IPeopleRepository
    {
        Task<IEnumerable<Person>> GetPeople();
    }
}
