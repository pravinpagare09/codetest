﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Agl.Business;
using Agl.Business.Enum;
using Agl.DataAccess;
using Agl.Model;

namespace AGLApp
{
    class Program
    {
        static void Main(string[] args)
        {
            RunAsync().Wait();
            Console.ReadLine();
        }

        private static async Task RunAsync()
        {
            //IOC 
            CatEntity peopleRepository = new CatEntity(new PeopleRepository());

            Dictionary<Gender, IEnumerable<Pet>> petsByGender = await peopleRepository.GetCatsByOwnerGender();
            foreach (var petWithOwnerGender in petsByGender)
            {
                Console.WriteLine(petWithOwnerGender.Key);
                Console.WriteLine("********");
                foreach (Pet pet in petWithOwnerGender.Value)
                {
                    Console.WriteLine(pet.Name);
                }
                Console.WriteLine("");
            }

        }
    }
}
