﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Agl.Business.Enum;
using Agl.DataAccess;
using Agl.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Agl.Business.Test
{
    [TestClass()]
    public class CatRepositoryTests
    {
        [TestMethod]
        public async Task GetCatsByMaleOwner()
        {
            // Arrange
            CatEntity catEntity = new CatEntity(new PeopleRepository());
            Dictionary<Gender, IEnumerable<Pet>> result = await catEntity.GetCatsByOwnerGender();

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(result[Gender.Male]);
            Assert.AreEqual("Garfield", result[Gender.Male].ToList()[0].Name);
        }

        [TestMethod]
        public async Task GetCatsByFeMaleOwner()
        {
            // Arrange
            CatEntity catEntity = new CatEntity(new PeopleRepository());
            Dictionary<Gender, IEnumerable<Pet>> result = await catEntity.GetCatsByOwnerGender();

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(result[Gender.Female]);
            Assert.AreEqual("Garfield", result[Gender.Female].ToList()[0].Name);
        }

    }
}
