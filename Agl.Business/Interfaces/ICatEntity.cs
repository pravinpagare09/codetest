﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Agl.Business.Enum;
using Agl.Model;

namespace Agl.Business.Interfaces
{
    interface ICatEntity
    {
        Task<Dictionary<Gender, IEnumerable<Pet>>> GetCatsByOwnerGender();
    }
}
